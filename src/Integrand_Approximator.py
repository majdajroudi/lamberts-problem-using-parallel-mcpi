import numpy as np

def G(D,r,final_time,mu):
        global G_total
        N = len(D)
        G_list = np.zeros((N,1),dtype = float)
        for i in range(N):
            g = -(final_time/2)* mu*D[i][0]/(r[i]**3)
            G_list[i][0] = g
        return G_list