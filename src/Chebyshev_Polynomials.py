from mpi4py import MPI
import random
import numpy as np
import time
import math
from Tau import tau
from helper_functions import split

comm = MPI.COMM_WORLD
rank = comm.rank
size = comm.size

def T(N):
    Cx= []
    if rank == 0:
        t = tau(N)
        splitted_t = split(t, size)
        
        for core in range(1,size):
            comm.send(splitted_t[core], dest= core, tag=22)
            
        for k in splitted_t[0]:
            T = [1,k]
            for j in range(2,N+1):
                Tn = 2*k*T[j-1] - T[j-2]
                T.append(Tn)
            Cx.append(T)
        
    else:
        for i in range(1,size):
            splitted_t = comm.recv(source= 0, tag=22)
            for k in splitted_t:
                T = [1, k]
                for j in range(2,N+1):
                    Tn = 2*k*T[j-1] - T[j-2]
                    T.append(Tn)
                Cx.append(T)
            comm.send(Cx, dest= 0, tag= 11)
            return f"data was sent from {rank} to 0"
    if rank == 0:
        for core in range(1,size):
            Cx_1 = comm.recv(source= core, tag= 11)
            for row in Cx_1:
                Cx.append(row)
        return Cx