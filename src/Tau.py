import numpy as np
import math

def tau(N):
        tau_list = []
        for i in range(N+1):
            t = -math.cos((i*math.pi)/N)
            tau_list.append(t)
        return tau_list