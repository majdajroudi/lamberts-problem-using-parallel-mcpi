# -*- coding: utf-8 -*-
"""
Created on Wed May 13 15:04:28 2020

@author: Majd
"""

from mpi4py import MPI
import random
import numpy as np
import time
import math
import Chebyshev_Polynomials
import helper_functions
import constant_matrices
from Integrand_Approximator import G
from Coefficient import F

# Start the communication word and find the size of the problem 
comm = MPI.COMM_WORLD
rank = comm.rank
size = comm.size
nodes_num = 100

# Boundary Conditions and Constants
# Values of the boundary conditions are chosen random for the sake of experiment
T0 = 0 # initial time
Tf = 360 # final time
mu = 5972e24*1000/(5972e24+1000) # Standard gravitational parameter

IC_x = [[500]] # initial X
IC_y = [[100]] # initial Y
IC_z = [[210]] # initial Z

# Make all values of the list equal to 0 except for the first and final value
for i in range(nodes_num-1):
    IC_x.append([0])
    IC_y.append([0])
    IC_z.append([0])  
IC_x.append([-1460]) # final X
IC_y.append([250]) # final Y
IC_z.append([700]) # final Z

IC_r = []
for i in range(nodes_num):
    IC_r.append([0])
IC_r.append([19])

# Functions and conditions that are defined in the master processor
if rank==0:
    start_time = time.time()
    print(f"size {size}")
    results_file = open("Sequent_position_results.txt","w")
    results_file_v = open("Sequent_velocity_results.txt", 'w')  

Cheb = Chebyshev_Polynomials.T(nodes_num) 

if rank == 0:
    def integrand(f,N):
        integ = []
        t = Cheb
        for n in range(N+1):
            S = 0
            for k in range(N):
                S += f[k][0]*t[n][k]
            integ.append([S])
        return integ
    Ca = np.dot(np.dot(constant_matrices.R(nodes_num),constant_matrices.S(nodes_num)),np.dot(Cheb,constant_matrices.V(nodes_num)))

def Sol_update(N):
    #global G_total
    F_total = 0 
    #global integrand_total
    if rank == 0:
        x = helper_functions.X_0(N)
        y = helper_functions.Y_0(N)
        z = helper_functions.Z_0(N)
        tolerance = 0.0001
        vx = helper_functions.VX_0(N)
        vy = helper_functions.VY_0(N)
        vz = helper_functions.VZ_0(N)

    for i in range(1000):
        # Norm matrix generation
        if rank == 0:
            
            r = helper_functions.norm(x,y,z)
    
            x_old = x[:]
            y_old = y[:]
            z_old = z[:]
            
            # G calculation
            g_start = time.time()
            g_x = G(x,r,Tf,mu)
            g_y = G(y,r,Tf,mu)
            g_z = G(z,r,Tf,mu)
            g_end = time.time()
            
        # Coefficient calculation
        
        if rank != 0:
            g_x = 1
            g_y = 1
            g_z = 1
        
        F_start = time.time()
        f_x = F(g_x,N,Cheb)
        f_y = F(g_y,N,Cheb)
        f_z = F(g_z,N,Cheb)
    
        F_finish = time.time()
        F_total += F_finish - F_start
        # Integrand approximation
        if rank == 0:
            integ_x = integrand(f_x,N)
            integ_y = integrand(f_y,N)
            integ_z = integrand(f_z,N)
            const = np.dot(Cheb,Ca)
        
            # Beta Calculation
            vx = np.dot(const,integ_x) + np.dot(Cheb,IC_r)
            vy = np.dot(const,integ_y) + np.dot(Cheb,IC_r)
            vz = np.dot(const,integ_z) + np.dot(Cheb,IC_r)
            
            # Dim. update
            x = (Tf/2)*np.dot(const,vx) + np.dot(Cheb,IC_x)
            y = (Tf/2)*np.dot(const,vy) + np.dot(Cheb,IC_y)
            z = (Tf/2)*np.dot(const,vz) + np.dot(Cheb,IC_z)
            
            e_x = []
            e_y = []
            e_z = []
            
            for k in range(len(x)):
                e_x.append(x[k][0]-x_old[k][0])
                e_y.append(y[k][0]-y_old[k][0])
                e_z.append(z[k][0]-z_old[k][0])
            
            
            if all(abs(x) < tolerance for x in e_x):
                if all(abs(x) < tolerance for x in e_y):
                    if all(abs(x) < tolerance for x in e_z):
                        for core in range(1,size):
                            comm.send("Done", dest= core, tag=88)
                        
                        results_file.write("X= \n")
                        results_file.write(np.array_str(x))
                        results_file.write("\n\nY= \n")
                        results_file.write(np.array_str(y))
                        results_file.write("\n\nZ= \n")
                        results_file.write(np.array_str(z))
                        results_file.close()
                        
                        results_file_v.write("Vx= \n")
                        results_file_v.write(np.array_str(vx))
                        results_file_v.write("\n\nVy= \n")
                        results_file_v.write(np.array_str(vy))
                        results_file_v.write("\n\nVz= \n")
                        results_file_v.write(np.array_str(vz))
                        results_file_v.close()
                    
                        print('###### -- Parallel-- ######')
                        finish_time = time.time()
                        print(f'G spends {g_end - g_start} secs')
                        print(f'F spends {F_total} secs')
                        print(f'single F run spends {F_finish-F_start} secs')
                        print('Iterations before convergence',i)
                        print('time = ',finish_time-start_time)
            
                        return "check the files to see the results"
            else:
                for core in range(1,size):
                    comm.send("Not Done", dest= core, tag=88)
            
        else:
            msg = comm.recv(source=0, tag=88)
            if msg == "Done":
                return


print(Sol_update(nodes_num))