import numpy as np

# Calculate R
def R(N):
    diag = [1]
    for r in range(1,N+1):
        i = 1/(2*r)
        diag.append(i)
    R_mx = np.diag(diag)
    return R_mx

#calculate V
def V(N):
    diag = [1/N]
    for i in range (1,N):
        diag.append(2/N)
    diag.append(1/N)
    V_mx = np.diag(diag)
    return V_mx

# calculate S
def S(N):
    f_row = [1,-0.5]
    for k in range(3,N+1):
        x = (((-1)**k)*((1/(k-2))-(1/k)))
        
        f_row.append(x)
    S_mx = [f_row]
    g = [1,0,-1]
    n = len(g)
    diag = np.zeros((N-2,N+n-3),dtype=int)
    for x in range(N-2):
        diag[x][x:x+n] = g
    for i in diag:
        S_mx.append(list(i))
    
    l_row = np.zeros((2,N),dtype=int)
    l_row[0][-2] = 1
    l_row[1][-1] = 1
    for i in l_row:
        S_mx.append(list(i))
    for i in S_mx:
        i.append(0)
    return S_mx