import random
import numpy as np

def X_0(N):
        X = []
        for i in range(N+1):
            X.append([random.randint(50,60)])
        return X
    
def Y_0(N):
    Y = []
    for i in range(N+1):
        Y.append([random.randint(50,60)])
    return Y
    
def Z_0(N):
    Z = []
    for i in range(N+1):
        Z.append([random.randint(50,60)])
    return Z

def VX_0(N):
    X = []
    for i in range(N+1):
        X.append([random.randint(-100,25000)])
    return X

def VY_0(N):
    Y = []
    for i in range(N+1):
        Y.append([random.randint(-10000,25000)])
    return Y
    
def VZ_0(N):
    Z = []
    for i in range(N+1):
        Z.append([random.randint(-100,25000)])
    return Z

def norm(x,y,z):
    r = []
    for i in range(len(x)):
        r.append(np.linalg.norm([x[i][0],y[i][0],z[i][0]]))
    
    return r

def split(data,coreN):
        N = len(data)
        element_size = int(N/coreN)
        splitted = []
        lim = 0
        for core in range(coreN-1):
            lim_e = lim + element_size
            sub_data = data[lim:lim_e]
            splitted.append(sub_data)
            lim += element_size
        
        sub_data = data[lim:]
        splitted.append(sub_data)
        return splitted

def vertical_split(data, n):
    transposed_data = np.array(data).transpose().tolist()
    splitted_data = split(transposed_data,size)
    retransposed = np.array(splitted_data).transpose().tolist()
    return retransposed