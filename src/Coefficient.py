from mpi4py import MPI
import numpy as np
import helper_functions

comm = MPI.COMM_WORLD
rank = comm.rank
size = comm.size

def F(fun,N, Cx):
    if size > 1:
        if rank == 0 :
            F_list = []
            splitted_t = helper_functions.split(Cx,size)
            t = splitted_t[0]
            for core in range(1,size):
                comm.send(splitted_t[core], dest = core, tag=44)
                comm.send(fun, dest = core, tag= 99)
            final_F = []
            for n in range(len(t)):
                S = 0
                for k in range(len(fun)):
                    if n == 0:
                        c = N+1
                        if k == 0 or N:
                            S += 0.5*fun[k][0]*t[n][k]
                        else:
                            S += fun[k][0]*t[n][k]
                    else:
                        c = N+1/2
                        if k == 0 or N:
                            S += 0.5*fun[k][0]*t[n][k]
                        else:
                            S += fun[k][0]*t[n][k]
                F = S/c
                F_list.append(F)
            
            for i in F_list:
                s = float(i)
                final_F.append([s])
        else:
            t = comm.recv(source= 0, tag=44)
            fun = comm.recv(source=0, tag = 99)
            F_list = []
            final_F = []
            for n in range(len(t)):
                S = 0
                for k in range(len(fun)):
                    c = N+1/2
                    if k == 0 or N:
                        S += 0.5*fun[k][0]*t[n][k]
                    else:
                        S += fun[k][0]*t[n][k]
                F = S/c
                F_list.append(F)
            
            for i in F_list:
                s = float(i)
                final_F.append([s])
            comm.send(final_F, dest = 0,tag = 55)
        
        if rank == 0:
            for core in range(1,size):
                final_F1 = comm.recv(source=core, tag=55)
                for row in final_F1:
                    final_F.append(row)
    
            return final_F
    else: 
        F_list = []
        t = Cx
        final_F = []
        for n in range(len(t)):
            S = 0
            for k in range(len(fun)):
                if n == 0:
                    c = N+1
                    if k == 0 or N:
                        S += 0.5*fun[k][0]*t[n][k]
                    else:
                        S += fun[k][0]*t[n][k]
                else:
                    c = N+1/2
                    if k == 0 or N:
                        S += 0.5*fun[k][0]*t[n][k]
                    else:
                        S += fun[k][0]*t[n][k]
            F = S/c
            F_list.append(F)
        
        for i in F_list:
            s = float(i)
            final_F.append([s])
        return final_F